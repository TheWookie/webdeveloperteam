package command;

import org.junit.Test;

import com.wookie.devteam.controller.command.Command;
import com.wookie.devteam.controller.command.impl.AddTaskCommand;
import com.wookie.devteam.controller.command.impl.LoginCommand;
import com.wookie.devteam.entities.Person;
import com.wookie.devteam.entities.builder.PersonBuilder;
import com.wookie.devteam.entities.builder.RoleBuilder;
import com.wookie.devteam.service.PersonService;
import com.wookie.devteam.service.factory.ServiceFactory;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginCommandTest {
	private ServiceFactory serviceFactory = mock(ServiceFactory.class);  
	private PersonService personService = mock(PersonService.class);
	private Command command = new LoginCommand().setServiceFactory(serviceFactory);
	private HttpServletRequest request = mock(HttpServletRequest.class);
	private HttpServletResponse response = mock(HttpServletResponse.class);
	
	@Test
	public void testExecute() throws ServletException, IOException, RuntimeException {
		String userLogin = "login";
		Person person = new PersonBuilder()
							.setId(1)
							.setRole(new RoleBuilder().setName("some role").build())
							.build();
		HttpSession session = mock(HttpSession.class); 
		
		when(request.getParameter(anyString()))
				.thenReturn(userLogin);
		when(request.getSession(true)).thenReturn(session);
		when(serviceFactory.getPersonService())
				.thenReturn(personService);
		when(personService.getPerson(userLogin))
				.thenReturn(person);
		
		command.execute(request, response);
		
		verify(session, times(2)).setAttribute(anyObject(), anyObject());
		
	}
}
