/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extractor;

import com.wookie.devteam.controller.command.impl.extractor.Extractor;
import java.util.HashSet;
import java.util.Set;
import javax.management.RuntimeErrorException;
import javax.servlet.http.HttpServletRequest;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import static org.mockito.Mockito.*;

/**
 *
 * @author wookie
 */
public class ExtractorTest {
    private HttpServletRequest request = mock(HttpServletRequest.class);
    private Extractor extractor = Extractor.getInstance();
    
    /**
     * Test of extractString method, of class Extractor.
     * Correct performance.
     */
    @Test
    public void testExtractStringCorrect() {
        String parameter = "cool parameter";
        String expResult = "OK";
        
        when(request.getParameter(parameter))
                .thenReturn(expResult);
        
        String result = extractor.extractString(request, parameter);
        
        assertEquals("Test extract String (correct)", expResult, result);
    }
    
    /**
     * Test of extractString method, of class Extractor.
     * Wrong performance.
     */
    @Test(expected = RuntimeException.class)
    public void testExtractStringWrong() {
        String parameter = "bad parameter";
        String expResult = "OK";
        
        when(request.getParameter(parameter))
                .thenReturn(null);
        
        String result = extractor.extractString(request, parameter);
        
        assertEquals("Test extract String (wrong)", expResult, result);
    }

    /**
     * Test of extractInt method, of class Extractor.
     * Correct performance.
     */
    @Test
    public void testExtractIntCorrect() {
        String parameter = "cool parameter";
        int expResult = 1;
        
        when(request.getParameter(parameter))
                .thenReturn("1");
        
        int result = extractor.extractInt(request, parameter);
        
        assertEquals("Test extract Int (correct)", expResult, result);

    }
 
    /**
     * Test of extractInt method, of class Extractor.
     * Wrong performance.
     */
    @Test(expected = RuntimeException.class)
    public void testExtractIntWrong() {
        String parameter = "bad parameter";
        int expResult = 1;
        
        when(request.getParameter(parameter))
                .thenReturn("incorrect");
        
        int result = extractor.extractInt(request, parameter);
        
        assertEquals("Test extract Int (wrong)", expResult, result);

    }
    
    
    @Test(timeout = 60)
    public void testExtractIntSet() {
        String parameter = "param";
        
        when(request.getParameter(parameter + 1))
                .thenReturn("1");
        when(request.getParameter(parameter + 2))
                .thenReturn("2");
        when(request.getParameter(parameter + 3))
                .thenReturn("3");
        when(request.getParameter(parameter + 4))
                .thenReturn(null);
        
        Set<Integer> expected = new HashSet<>();
        expected.add(1);
        expected.add(2);
        expected.add(3);
        
        Set<Integer> result = extractor.extractIntSet(request, parameter);
        
        assertEquals("Test extract Int Set", expected, result);
    }
    
    @Test
    @Ignore
    public void testExtractFutureDate() {
        String param = "param";
        String input = "2016-12-29";
        
        when(request.getParameter(param))
                .thenReturn(input);
        
        extractor.extractFutureDate(request, param);
    }
    
    @Test(expected = RuntimeException.class)
    @Ignore
    public void testExtractFutureDateWithException() {
        String param = "param";
        String input = "2016-12-01";
        
        when(request.getParameter(param))
                .thenReturn(input);
        
        extractor.extractFutureDate(request, param);
    }
    
    @Test
    public void testExtractEmailCorrect() {
    	String param = "param";
        String input = "foo@gmail.com";
        
        when(request.getParameter(param))
                .thenReturn(input);
        
        extractor.extractEmail(request, param);
    }
    
    @Test(expected = RuntimeException.class)
    public void testExtractEmailWrong() {
    	String param = "param";
        String input = "foo@.com";
        
        when(request.getParameter(param))
                .thenReturn(input);
        
        extractor.extractEmail(request, param);
    }
    
    @Test
    public void testPhoneNumberCorrect() {
    	String param = "param";
        String input = "123";
        
        when(request.getParameter(param))
                .thenReturn(input);
        
        extractor.extractPhoneNumber(request, param);
    }
    
    @Test(expected = RuntimeException.class)
    public void testPhoneNumberWrong() {
    	String param = "param";
        String input = "123a";
        
        when(request.getParameter(param))
                .thenReturn(input);
        
        extractor.extractPhoneNumber(request, param);
    }
    
    @Test(expected = RuntimeException.class)
    public void testPhoneNumberWrongNullParameter() {
    	String param = "param";
        String input = null;
        
        when(request.getParameter(param))
                .thenReturn(input);
        
        extractor.extractPhoneNumber(request, param);
    }
}


