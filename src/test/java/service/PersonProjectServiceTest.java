/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.wookie.devteam.constants.Attributes;
import com.wookie.devteam.constants.Roles;
import com.wookie.devteam.dao.DaoFactory;
import com.wookie.devteam.dao.PersonDao;
import com.wookie.devteam.dao.PersonProjectDao;
import com.wookie.devteam.dao.ProjectDao;
import com.wookie.devteam.dao.StatusDao;
import com.wookie.devteam.entities.Person;
import com.wookie.devteam.entities.PersonProject;
import com.wookie.devteam.entities.Project;
import com.wookie.devteam.entities.builder.PersonBuilder;
import com.wookie.devteam.entities.builder.PersonProjectBuilder;
import com.wookie.devteam.entities.builder.ProjectBuilder;
import com.wookie.devteam.entities.builder.RoleBuilder;
import com.wookie.devteam.entities.builder.StatusBuilder;
import com.wookie.devteam.service.PersonProjectService;
import com.wookie.devteam.service.PersonService;
import com.wookie.devteam.service.ProjectService;
import com.wookie.devteam.service.transactions.impl.JdbcTransactionManager;
import com.wookie.devteam.service.transactions.TransactionManager;
import dao.jdbc.TestJdbcDaoFactory;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

/**
 *
 * @author wookie
 */
public class PersonProjectServiceTest {
    private DaoFactory mockFactory = new DaoMockFactory();
    private PersonProjectDao personProjectDao = mockFactory.createPersonProjectDao();
    private PersonDao personDao = mockFactory.createPersonDao();
    private ProjectDao projectDao = mockFactory.createProjectDao();
    private StatusDao statusDao = mockFactory.createStatusDao();
    private TransactionManager transactionManager = new JdbcTransactionManager(mockFactory.getDataSource());
    private DaoFactory factory = mock(DaoFactory.class);
    private PersonProjectService service = new PersonProjectService(factory, transactionManager);
    
    @Test
    public void createPersonProjectFromRequestTest() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        
        when(factory.createPersonProjectDao()).
                thenReturn(personProjectDao);
        when(factory.createPersonDao()).
                thenReturn(personDao);
        when(factory.createProjectDao()).
                thenReturn(projectDao);
        when(factory.createStatusDao()).
                thenReturn(statusDao);
        
        when(personProjectDao.create(anyObject(), anyObject()))
                .thenReturn(null);
        when(projectDao.update(anyObject(), anyObject()))
                .thenReturn(true);
        when(projectDao.findById(anyObject(), eq(0)))
        		.thenReturn(new ProjectBuilder().setStatus(
        				new StatusBuilder().setName(Attributes.FREE_PROJECT).build())
        				.build());
        when(personDao.update(anyObject(), anyObject()))
                .thenReturn(true);
        when(statusDao.findByName(anyObject(), eq(Attributes.IN_PROGRESS_STATUS)))
                .thenReturn(new StatusBuilder().setId(1).setName("Status").build());
        
        Set<Integer> param = new HashSet<>();
        param.add(1);
        param.add(2);
        param.add(3);
        
        service.createPersonProjectFromRequest(param, 0, 0, new BigDecimal(0));
        
        verify(personProjectDao, times(4)).create(anyObject(), anyObject());
        verify(personDao, times(3)).update(anyObject(), anyObject());
        verify(projectDao, times(1)).update(anyObject(), anyObject());
    }
    
    @Test
    public void fetchDevsTest() {
    	PersonProjectService spyService = spy(service);
    	int expectedId = 2;
    	int projectId = 1;
    	PersonProject developer = new PersonProjectBuilder()
    			.setPerson(new PersonBuilder()
    					.setId(expectedId)
    					.setName("right person")
    					.setBirthday(new java.sql.Date(new Date().getTime()))
            	.setRole(new RoleBuilder()
                    .setName(Roles.DEV_ROLE)
                    .build())
            	.build())
    			.setProject(new ProjectBuilder()
    					.setId(1)
    					.setName("project")
    					.build())
    			.build();

    	Set<PersonProject> projects = new HashSet<>();
    	projects.add(new PersonProjectBuilder()
            .setPerson(new PersonBuilder()
                .setId(1)
                .setName("wrong person")
                .setRole(new RoleBuilder()
                        .setName(Roles.MANAGER_ROLE)
                        .build())
                .setBirthday(new java.sql.Date(new Date().getTime()))
                .build())
            .setProject(new ProjectBuilder()
                .setId(1)
                .setName("project")
                .build())
            .build());
    	projects.add(developer);
    	
    	doReturn(projects).when(spyService).getByProject(projectId);
    	
    	Map<PersonProject, Long> result = spyService.fetchDevs(projectId);
        
    	assertNotNull(result);
    	assertEquals(1, result.size());
    }
    
    @Test
//    @Ignore // TODO: test streams #2
    public void getByPersonTest() throws SQLException {
        Set<PersonProject> expReturn = new HashSet<>();
        expReturn.add(new PersonProjectBuilder()
                            .setPerson(new PersonBuilder().setId(1).build())
                            .setProject(new ProjectBuilder().setId(1).build())
                            .build());
        Person expPerson = new PersonBuilder()
                        .setId(1)
                        .setName("aaa")
                        .build();
        Project expProject = new ProjectBuilder()
                        .setId(1)
                        .setName("Test proj")
                        .build(); 
        
        when(factory.createPersonProjectDao()).
                thenReturn(personProjectDao);
        when(factory.createPersonDao()).
                thenReturn(personDao);
        when(factory.createProjectDao()).
                thenReturn(projectDao);
        when(personProjectDao.findByPerson(anyObject(), eq(1)))
                .thenReturn(expReturn);
        when(personDao.findById(any(), eq(1)))
                .thenReturn(expPerson);
        when(projectDao.findById(any(), eq(1)))
                .thenReturn(expProject);
     
        expReturn.iterator().next().setPerson(expPerson);
        expReturn.iterator().next().setProject(expProject);

        Set<PersonProject> result = service.getByPerson(1);
        assertNotNull(result);
        assertEquals("get by person test : ", expReturn, result);
    } 
    
}
