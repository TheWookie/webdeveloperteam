/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.wookie.devteam.dao.DaoFactory;
import com.wookie.devteam.dao.PersonDao;
import com.wookie.devteam.dao.PersonProjectDao;
import com.wookie.devteam.dao.ProjectDao;
import com.wookie.devteam.dao.StatusDao;
import com.wookie.devteam.dao.TaskDao;
import com.wookie.devteam.entities.Person;
import com.wookie.devteam.entities.builder.PersonBuilder;
import com.wookie.devteam.service.PersonProjectService;
import com.wookie.devteam.service.PersonService;
import com.wookie.devteam.service.transactions.impl.JdbcTransactionManager;
import com.wookie.devteam.service.transactions.TransactionManager;
import dao.jdbc.TestJdbcDaoFactory;
import java.util.HashSet;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author wookie
 */
public class PersonServiceTest {
    private DaoFactory mockFactory = new DaoMockFactory();
    
    private TransactionManager transactionManager = new JdbcTransactionManager(mockFactory.getDataSource());
    private DaoFactory factory = mock(DaoFactory.class);
    
    private PersonService personService = new PersonService(factory, transactionManager);

    @Test
    public void testCheckTruePassword() {
        String password = "pass";
        
        Person user = new PersonBuilder()
                .setPassword(password)
                .build();
        personService.checkPassword(user, password);
    }
    
    @Test(expected=RuntimeException.class)
    public void testCheckFalsePassword() {
        String password = "pass";
        
        Person user = new PersonBuilder()
                .setPassword(password)
                .build();
        personService.checkPassword(user, "wrong");
    }
    
}
