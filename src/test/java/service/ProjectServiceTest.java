/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.wookie.devteam.dao.DaoFactory;
import com.wookie.devteam.dao.PersonDao;
import com.wookie.devteam.dao.PersonProjectDao;
import com.wookie.devteam.dao.ProjectDao;
import com.wookie.devteam.dao.StatusDao;
import com.wookie.devteam.entities.Project;
import com.wookie.devteam.entities.builder.ProjectBuilder;
import com.wookie.devteam.service.PersonProjectService;
import com.wookie.devteam.service.ProjectService;
import com.wookie.devteam.service.transactions.TransactionManager;
import com.wookie.devteam.service.transactions.impl.JdbcTransactionManager;

import dao.jdbc.TestJdbcDaoFactory;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Date;

/**
 *
 * @author wookie
 */
public class ProjectServiceTest {
	private DaoFactory mockFactory = new DaoMockFactory();
    private PersonProjectDao personProjectDao = mockFactory.createPersonProjectDao();
    private ProjectDao projectDao = mockFactory.createProjectDao();
    private TransactionManager transactionManager = new JdbcTransactionManager(mockFactory.getDataSource());
    private DaoFactory factory = mock(DaoFactory.class);
   
    private ProjectService projectService = new ProjectService(factory, transactionManager);

    @Test
	public void testCreate() {
		String name = "project name";
		Date deadline = new Date();
		int clientId = 1;
		
		Project project = new ProjectBuilder()
                	.setName(name)
                	.setDeadline(deadline)
                	.build();
		
		when(factory.createPersonProjectDao())
				.thenReturn(personProjectDao);
		when(factory.createProjectDao())
				.thenReturn(projectDao);
		
		when(projectDao.findByName(anyObject(), eq(project)))
				.thenReturn(null);
		when(projectDao.create(anyObject(), eq(project)))
				.thenReturn(project);
		
		assertEquals(project, projectService.create(name, deadline, clientId));
	}
  
	@Test(expected = RuntimeException.class)
	public void testCreateWithExistingProjectInDatabase() {
		String name = "project name";
		Date deadline = new Date();
		int clientId = 1;
		 
		Project project = new ProjectBuilder()
                	.setName(name)
                	.setDeadline(deadline)
                	.build();
		
		when(factory.createPersonProjectDao())
				.thenReturn(personProjectDao);
		when(factory.createProjectDao())
				.thenReturn(projectDao);
		
		when(projectDao.findByName(anyObject(), project))
				.thenReturn(project);
		
		projectService.create(name, deadline, clientId);
	}
}

