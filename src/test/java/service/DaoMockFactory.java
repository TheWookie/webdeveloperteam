package service;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.wookie.devteam.dao.DaoFactory;
import com.wookie.devteam.dao.PersonDao;
import com.wookie.devteam.dao.PersonProjectDao;
import com.wookie.devteam.dao.ProjectDao;
import com.wookie.devteam.dao.ProjectQualificationDao;
import com.wookie.devteam.dao.QualificationDao;
import com.wookie.devteam.dao.RoleDao;
import com.wookie.devteam.dao.StatusDao;
import com.wookie.devteam.dao.TaskDao;
import com.wookie.devteam.dao.jdbc.JdbcStatusDao;

public class DaoMockFactory extends DaoFactory {

	@Override
	public StatusDao createStatusDao() {
    	return mock(JdbcStatusDao.class);
	}

	@Override
	public ProjectQualificationDao createProjectQualificationDao() {
    	return mock(ProjectQualificationDao.class);
	}
	
	@Override
	public TaskDao createTaskDao() {
    	return mock(TaskDao.class);
	}

	@Override
	public RoleDao createRoleDao() {
    	return mock(RoleDao.class);
	}

	@Override
	public QualificationDao createQualificationDao() {
    	return mock(QualificationDao.class);
	}

	@Override
	public ProjectDao createProjectDao() {
    	return mock(ProjectDao.class);
	}

	@Override
	public PersonProjectDao createPersonProjectDao() {
    	return mock(PersonProjectDao.class);
	}

	@Override
	public PersonDao createPersonDao() {
    	return mock(PersonDao.class);
	}

	@Override
	public DataSource getDataSource() {
		try {
			DataSource ds = mock(DataSource.class);
			Connection fakeConnection = getConnection();
			when(ds.getConnection()).thenReturn(fakeConnection);
			return ds;
		} catch (SQLException ex) {
			return null;
		}
   
	}

	@Override
	public Connection getConnection() {
		try {
			Connection fakeConnection = mock(Connection.class);
			doNothing().when(fakeConnection).commit();
			doNothing().when(fakeConnection).setAutoCommit(false);
			doNothing().when(fakeConnection).close();
			return fakeConnection;
		} catch (SQLException ex) {
			return null;
		}
	}
}
