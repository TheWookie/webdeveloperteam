/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.jdbc;

import com.wookie.devteam.dao.RoleDao;
import com.wookie.devteam.dao.jdbc.JdbcDaoFactory;
import com.wookie.devteam.dao.jdbc.JdbcRoleDao;
import com.wookie.devteam.entities.Role;
import com.wookie.devteam.entities.builder.RoleBuilder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wookie
 */
public class TestJdbcRoleDao {
    private class DaoTest extends JdbcRoleDao {

    }
    
    private TestJdbcDaoFactory factory = new TestJdbcDaoFactory() {
        @Override
        public RoleDao createRoleDao() {
            return new DaoTest();
        }
    };
    
    private Connection connection;
    {
        try {
            connection = factory.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException ex) {
        }
    }
    
    private DaoTest dao = (DaoTest)factory.createRoleDao();
//    {
//        dao.setConnection(connection);
//    }
    
    @Test
    public void testFindById() throws SQLException {
        Role instance = dao.findById(connection, 1);
        assertNotNull("Create ", instance);
    }
    
    @Test 
    public void testCreate() throws SQLException {
        Role r = dao.create(connection, new RoleBuilder().setName("aaa").build()); 
        System.out.println("testCreate Result: " + r.getId());
        connection.rollback();
    }
    
//    @Test
//    public void testDelete() throws SQLException {
//        boolean result = dao.delete(connection, new RoleBuilder().setId(5).build());
//        assertEquals("test delete", true, result);
//        connection.rollback();
//    }


    @Test
    public void testFindAll() throws SQLException {
        Set<Role> instance = dao.findAll(connection);
        assertEquals("Find all test", false, instance.isEmpty());
    }

    @Test
    public void testUpdate() throws SQLException {
        boolean result = dao.update(connection,
                        new RoleBuilder()
                            .setId(1)
                            .setName("asd")
                            .build());
        
        assertEquals("test update", true, result);
        connection.rollback();
    }
}
