/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.jdbc;

import com.wookie.devteam.dao.DaoFactory;
import com.wookie.devteam.dao.PersonDao;
import com.wookie.devteam.dao.PersonProjectDao;
import com.wookie.devteam.dao.ProjectDao;
import com.wookie.devteam.dao.ProjectQualificationDao;
import com.wookie.devteam.dao.QualificationDao;
import com.wookie.devteam.dao.RoleDao;
import com.wookie.devteam.dao.StatusDao;
import com.wookie.devteam.dao.TaskDao;
import com.wookie.devteam.dao.jdbc.JdbcDaoFactory;
import com.wookie.devteam.dao.jdbc.JdbcPersonProjectDao;
import com.wookie.devteam.dao.jdbc.JdbcStatusDao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.mockito.Mock;
import static org.mockito.Mockito.*;

/**
 *
 * @author wookie
 */
public class TestJdbcDaoFactory extends DaoFactory {
     public static final String HOST = "jdbc:mysql://localhost:3306/DevDBtest"
            + "?useUnicode=true&characterEncoding=UTF-8&characterSetResults=UTF-8"
            + "&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&"
            + "serverTimezone=UTC&useSSL=false";
        public static final String ROOT = "root";
        public static final String PASSWORD = "apocalypse";
    
        public Connection getConnection() {
            try {
                try {
                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                
                return DriverManager.getConnection(HOST, ROOT, PASSWORD) ;
            } catch (SQLException ex) {
                return null;
            }
        }

    @Override
    public StatusDao createStatusDao() {
        return mock(JdbcStatusDao.class);
    }

    @Override
    public ProjectQualificationDao createProjectQualificationDao() {
        return mock(ProjectQualificationDao.class);
    }

    @Override
    public TaskDao createTaskDao() {
        return mock(TaskDao.class);
    }

    @Override
    public RoleDao createRoleDao() {
        return mock(RoleDao.class);
    }

    @Override
    public QualificationDao createQualificationDao() {
        return mock(QualificationDao.class);
    }

    @Override
    public ProjectDao createProjectDao() {
        return mock(ProjectDao.class);
    }

    @Override
    public PersonProjectDao createPersonProjectDao() {
        return mock(PersonProjectDao.class);
//        return spy(new JdbcPersonProjectDao());
    }

    @Override
    public PersonDao createPersonDao() {
        return mock(PersonDao.class);
    }

    @Override
    public DataSource getDataSource() {
         try {
             DataSource ds = mock(DataSource.class);
             when(ds.getConnection()).thenReturn(this.getConnection());
             return ds;
         } catch (SQLException ex) {
             return null;
         }
        
    }
//    
//    @Override
//    public DataSource getDataSource() {
//        try {
//            DataSource ds = mock(DataSource.class);
//            Connection fakeConnection = mock(Connection.class);
//            doNothing().when(fakeConnection).commit();
//            doNothing().when(fakeConnection).setAutoCommit(false);
//            doNothing().when(fakeConnection).close();
//            when(ds.getConnection()).thenReturn(fakeConnection);
//            return ds;
//        } catch (SQLException ex) {
//            return null;
//        }
//       
//   }
}
