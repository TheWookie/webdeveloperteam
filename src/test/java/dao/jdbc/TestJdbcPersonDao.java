/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.jdbc;

import com.wookie.devteam.dao.DaoFactory;
import com.wookie.devteam.dao.PersonDao;
import com.wookie.devteam.dao.jdbc.JdbcPersonDao;
import com.wookie.devteam.entities.Person;
import com.wookie.devteam.entities.Qualification;
import com.wookie.devteam.entities.builder.PersonBuilder;
import com.wookie.devteam.entities.builder.QualificationBuilder;
import com.wookie.devteam.entities.builder.RoleBuilder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wookie
 */
public class TestJdbcPersonDao {
    private class DaoTest extends JdbcPersonDao {
        
    }
    
    private TestJdbcDaoFactory factory = new TestJdbcDaoFactory() {
        @Override
        public PersonDao createPersonDao() {
            return new DaoTest();
        }
    };
    
    private Connection connection;
    {
        try {
            connection = factory.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException ex) {
        }
    }
    
    private DaoTest dao = (DaoTest)factory.createPersonDao();
//    {
//        dao.setConnection(connection);
//    }
 
    @Test
    public void testFindById() throws SQLException {
        Person instance = dao.findById(connection, 1);
        assertNotNull("Create ", instance);
        System.out.println("testFindById Result: " + instance);
    }
    
    @Test
    public void testFindByLogin() throws SQLException {
        Person instance = dao.findByLogin(connection, "client");
        assertNotNull("Create ", instance);
        System.out.println("testFindById Result: " + instance);
    }
    
    @Test
    public void testFindFreeDevs() {
        assertNotNull(dao.findFreeDevs(connection));
        assertEquals("Count of entries: ", 4, dao.findFreeDevs(connection).size());
    }
    
    @Test 
    public void testCreate() throws SQLException {
        Person r = dao.create(connection, 
                new PersonBuilder()
                .setName("aaa")
                .setSurname("bbb")
                .setPhone("00420")
                .setQualification(new QualificationBuilder().setId(1).build())
                .setRole(new RoleBuilder().setId(1).build())
//                .setStatus(true)
                .setBirthday(new java.sql.Date(new Date().getTime()))
                .setLogin("loG")
                .setPassword("passswd")
                .setEmail("aa@bb.com")
                .build()
        ); 
        
        System.out.println("test create result " + r.getId());
        
        connection.rollback();
    }
    
    @Test
    public void testDelete() throws SQLException {
        dao.delete(connection, new PersonBuilder().setId(9).build());
        connection.rollback();
    }

    @Test
    public void testFindAll() throws SQLException {
        Set<Person> instance = dao.findAll(connection);
        assertEquals("Find all test", false, instance.isEmpty());
        System.out.println("testFindAll Result: " + instance.iterator().next().getName());
    }

    @Test
    public void testUpdate() throws SQLException {
        Person r = new PersonBuilder()
                .setId(1)
                .setName("aaasd")
                .setSurname("bbbsd")
                .setPhone("004201")
                .setQualification(new QualificationBuilder().setId(1).build())
                .setRole(new RoleBuilder().setId(1).build())
                .setStatus(true)
                .setBirthday(new java.sql.Date(new Date().getTime()))
                .setLogin("loG")
                .setPassword("pass")
                .setEmail("aa@bb.com")
                .build(); 
        boolean result = dao.update(connection, r);
        
        assertEquals("test update result", true, result);
        
        connection.commit();
    }
}
