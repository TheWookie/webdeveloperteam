/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.jdbc;

import com.wookie.devteam.dao.TaskDao;
import com.wookie.devteam.dao.jdbc.JdbcTaskDao;
import com.wookie.devteam.entities.Task;
import com.wookie.devteam.entities.builder.ProjectBuilder;
import com.wookie.devteam.entities.builder.StatusBuilder;
import com.wookie.devteam.entities.builder.TaskBuilder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wookie
 */
public class TestJdbcTaskDao {
    private class DaoTest extends JdbcTaskDao {

    }
    
    private TestJdbcDaoFactory factory = new TestJdbcDaoFactory() {
        @Override
        public TaskDao createTaskDao() {
            return new DaoTest();
        }
    };
    
    private Connection connection;
    {
        try {
            connection = factory.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException ex) {
        }
    }
    
    private DaoTest dao = (DaoTest)factory.createTaskDao();
//    {
//        dao.setConnection(connection);
//    }
 
    @Test
    public void testFindById() throws SQLException {
        Task instance = dao.findById(connection, 1);
        assertNotNull("Find by id ", instance);
    }
    
    @Test 
    public void testCreate() throws SQLException {
        Task r = dao.create(connection, new TaskBuilder()
                .setText("рус")
                .setProject(new ProjectBuilder().setId(1).build())
                .build()); 
        System.out.println("testCreate Result: " + r.getId());
        
//        connection.commit();
        connection.rollback();
    }
    
    @Test
    public void testDelete() throws SQLException {
        boolean result = dao.delete(connection, new TaskBuilder().setId(4).build());
        assertEquals("test delete", true, result);
        connection.rollback();    
    }

    @Test
    public void testFindAll() throws SQLException {
        Set<Task> instance = dao.findAll(connection);
        assertEquals("Find all test", false, instance.isEmpty());
    }

    @Test
    public void testUpdate() throws SQLException {
        boolean result = dao.update(connection, new TaskBuilder()
                .setId(4)
                .setText("aaa")
                .setProject(new ProjectBuilder().setId(1).build())
                .setStatus(new StatusBuilder().setId(2).build())
                .build());
        
        assertEquals("test delete", true, result);
        connection.rollback(); 
    }
}
