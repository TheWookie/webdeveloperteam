/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.jdbc;

import com.wookie.devteam.dao.QualificationDao;
import com.wookie.devteam.dao.jdbc.JdbcQualificationDao;
import com.wookie.devteam.entities.Qualification;
import com.wookie.devteam.entities.builder.QualificationBuilder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wookie
 */
public class TestJdbcQualificationDao {
    private class DaoTest extends JdbcQualificationDao {

    }
    
    private TestJdbcDaoFactory factory = new TestJdbcDaoFactory() {
        @Override
        public QualificationDao createQualificationDao() {
            return new DaoTest();
        }
    };
    
    private Connection connection;
    {
        try {
            connection = factory.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException ex) {
        }
    }
    
    private DaoTest dao = (DaoTest)factory.createQualificationDao();
//    {
//        dao.setConnection(connection);
//    }
 
    @Test
    public void testFindById() throws SQLException {
        Qualification instance = dao.findById(connection, 1);
        assertNotNull("test find by ID ", instance);
        System.out.println("test find by ID Result: " + instance.getName());
    }
    
    @Test 
    public void testCreate() throws SQLException {
        Qualification instance = dao.create(connection, new QualificationBuilder().setName("aaa").build()); 
        assertNotNull("test create ", instance);
        System.out.println("testCreate Result: " + instance.getId());
        connection.rollback();
    }
    
//    @Test
//    public void testDelete() throws SQLException {
//        boolean result = dao.delete(new QualificationBuilder().setId(5).build());
//        assertEquals("test delete result ", true, result);
//        connection.rollback();
//    }


    @Test
    public void testFindAll() throws SQLException {
        Set<Qualification> instance = dao.findAll(connection);
        assertEquals("Find all test", false, instance.isEmpty());
    }

    @Test
    public void testUpdate() throws SQLException {
        boolean result = dao.update(
                connection, 
                new QualificationBuilder()
                .setId(4)
                .setName("asd")
                .build()
        );
        
        assertEquals("test udate result ", true, result);
        connection.rollback();
    }
}
