/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.jdbc;

import com.wookie.devteam.dao.PersonProjectDao;
import com.wookie.devteam.dao.jdbc.JdbcPersonProjectDao;
import com.wookie.devteam.entities.PersonProject;
import com.wookie.devteam.entities.builder.PersonBuilder;
import com.wookie.devteam.entities.builder.PersonProjectBuilder;
import com.wookie.devteam.entities.builder.ProjectBuilder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wookie
 */
public class TestJdbcPersonProjectDao {
    private class DaoTest extends JdbcPersonProjectDao {
       
    }
    
    private TestJdbcDaoFactory factory = new TestJdbcDaoFactory() {
        @Override
        public PersonProjectDao createPersonProjectDao() {
            return new DaoTest();
        }
    };
    
    private Connection connection;
    {
        try {
            connection = factory.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException ex) {
        }
    }
    
    private DaoTest dao = (DaoTest)factory.createPersonProjectDao();
//    {
//        dao.setConnection(connection);
//    }
 
    @Test
    public void testFindById() throws SQLException {
        PersonProject instance = dao.findById(connection, 2, 1);
        assertNotNull("Create ", instance);
        assertEquals("test find by Id result", new Integer(12), instance.getTime());
    }
    
    @Test 
    public void testCreate() throws SQLException {
        PersonProject r = dao.create(connection,
                new PersonProjectBuilder()
                .setPerson(new PersonBuilder().setId(1).build())
                .setProject(new ProjectBuilder().setId(1).build())
                .setTime(11)
                .build());
                
        connection.rollback();
    }
    
    @Test
    public void testDelete() throws SQLException {
        boolean result = dao.delete(connection, 
                new PersonProjectBuilder()
                .setPerson(new PersonBuilder().setId(1).build())
                .setProject(new ProjectBuilder().setId(2).build())
                .build());
        
        assertEquals("test delete ", true, result);
        connection.rollback();
    }

    @Test
    public void testFindAll() throws SQLException {
        Set<PersonProject> instance = dao.findAll(connection);
        assertEquals("Find all test", false, instance.isEmpty());
    }

    @Test
    public void testUpdate() throws SQLException {
        dao.update(connection, new PersonProjectBuilder()
                .setPerson(new PersonBuilder().setId(1).build())
                .setProject(new ProjectBuilder().setId(1).build())
                .setTime(111)
                .build());
        
        connection.rollback();
    }
}
