/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.jdbc;

import com.wookie.devteam.dao.ProjectDao;
import com.wookie.devteam.dao.jdbc.JdbcProjectDao;
import com.wookie.devteam.entities.Project;
import com.wookie.devteam.entities.builder.ProjectBuilder;
import com.wookie.devteam.entities.builder.StatusBuilder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wookie
 */
public class TestJdbcProjectDao {
    private class DaoTest extends JdbcProjectDao {

    }
    
    private TestJdbcDaoFactory factory = new TestJdbcDaoFactory() {
        @Override
        public ProjectDao createProjectDao() {
            return new DaoTest();
        }
    };
    
    private Connection connection;
    {
        try {
            connection = factory.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException ex) {
        }
    }
    
    private DaoTest dao = (DaoTest)factory.createProjectDao();
//    {
//        dao.setConnection(connection);
//    }
 
    @Test
    public void testFindById() throws SQLException {
        Project instance = dao.findById(connection, 1);
        assertNotNull("find by id (not null) ", instance);
        assertEquals("find by id result ", "Ep", instance.getName());
    }
    
    @Test 
    public void testCreate() throws SQLException {
        Project instance = dao.create(connection,
                new ProjectBuilder()
                .setName("namee")
                .setDeadline(new java.sql.Date(new Date().getTime()))
                .build()); 
        assertNotNull("test create result ", instance);
        connection.rollback();
    }
    
    @Test
    public void testDelete() throws SQLException {
        boolean result = dao.delete(connection, new ProjectBuilder().setId(3).build());
        assertEquals("test delet result", true, result);
        connection.rollback();
    }

    @Test
    public void testFindAll() throws SQLException {
        Set<Project> instance = dao.findAll(connection);
        assertEquals("Find all test", false, instance.isEmpty());
    }

    @Test
    public void testUpdate() throws SQLException {
        boolean result = dao.update(connection, new ProjectBuilder()
                .setId(2)
                .setName("namee")
                .setDeadline(new java.sql.Date(new Date().getTime()))
                .setStatus(new StatusBuilder().setId(1).build())
                .build());
        assertEquals("test delete result", true, result);
        connection.rollback();
    }
}
