/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.jdbc;

import com.wookie.devteam.constants.Attributes;
import com.wookie.devteam.dao.StatusDao;
import com.wookie.devteam.dao.jdbc.JdbcStatusDao;
import com.wookie.devteam.entities.Status;
import com.wookie.devteam.entities.builder.StatusBuilder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wookie
 */
public class TestJdbcStatusDao {
    private class DaoTest extends JdbcStatusDao {

    }
    
    private TestJdbcDaoFactory factory = new TestJdbcDaoFactory() {
        @Override
        public StatusDao createStatusDao() {
            return new DaoTest();
        }
    };
    
    private Connection connection;
    {
        try {
            connection = factory.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException ex) {
        }
    }
    
    private DaoTest dao = (DaoTest)factory.createStatusDao();
//    {
//        dao.setConnection(connection);
//    }
 
    @Test
    public void testFindById() throws SQLException {
        Status instance = dao.findById(connection, 1);
        assertNotNull("Create ", instance);
    }
    
    @Test
    public void testFindByName() throws SQLException {
        Status instance = dao.findByName(connection, Attributes.FREE_PROJECT);
        assertNotNull("Create ", instance);
    }
    
    @Test 
    public void testCreate() throws SQLException {
        Status r = dao.create(connection, new StatusBuilder().setName("aaa").build()); 
        System.out.println("testCreate Result: " + r.getId());
        connection.rollback();
    }
    
//    @Test
//    public void testDelete() throws SQLException {
//        boolean result = dao.delete(connection, new StatusBuilder().setId(1).build());
//        assertEquals("test delete", true, result);
//        connection.rollback();
//    }

    @Test
    public void testFindAll() throws SQLException {
        Set<Status> instance = dao.findAll(connection);
        assertEquals("Find all test", false, instance.isEmpty());
        System.out.println("testFindAll Result: " + instance.iterator().next().getName());
    }

//    @Test
//    public void testUpdate() throws SQLException {
//        boolean result = dao.update(connection, new StatusBuilder()
//                                .setId(1)
//                                .setName("asd")
//                                .build());
//        
//        assertEquals("test delete", true, result);
//        connection.rollback();
//    }
}
