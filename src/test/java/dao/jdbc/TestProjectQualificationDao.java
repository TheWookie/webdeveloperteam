/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.jdbc;

import com.wookie.devteam.dao.ProjectQualificationDao;
import com.wookie.devteam.dao.jdbc.JdbcProjectQualificationDao;
import com.wookie.devteam.entities.ProjectQualification;
import com.wookie.devteam.entities.builder.ProjectBuilder;
import com.wookie.devteam.entities.builder.QualificationBuilder;
import com.wookie.devteam.entities.builder.ProjectQualificationBuilder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wookie
 */
public class TestProjectQualificationDao {
    private class DaoTest extends JdbcProjectQualificationDao {

    }
    
    private TestJdbcDaoFactory factory = new TestJdbcDaoFactory() {
        @Override
        public ProjectQualificationDao createProjectQualificationDao() {
            return new DaoTest();
        }
    };
    
    private Connection connection;
    {
        try {
            connection = factory.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException ex) {
        }
    }
    
    private DaoTest dao = (DaoTest)factory.createProjectQualificationDao();
//    {
//        try {
//            dao.setConnection(TestJdbcDaoFactory.getConnection());
//        } catch (SQLException ex) {
//        }
//    }
 
    @Test
    public void testFindById() throws SQLException {
        ProjectQualification instance = dao.findById(connection, 1, 2);
        assertNotNull("Find by ID", instance);
        System.out.println("testFindById Result: " + 
                instance.getQualification().getId() + 
                instance.getProject().getId());
        System.out.println("testFindById Result(qualification): " + instance.getQualification().getName());
    }
    
    @Test 
    public void testCreate() throws SQLException {
        ProjectQualification instance = dao.create(connection,
                new ProjectQualificationBuilder()
                .setQualification(new QualificationBuilder().setId(2).build())
                .setProject(new ProjectBuilder().setId(2).build())
                .setCount(2)
                .build()); 
        assertNotNull("test create", instance);
        connection.rollback();
    }
    
    @Test
    public void testDelete() throws SQLException {
        boolean result = dao.delete(connection, 
                new ProjectQualificationBuilder()
                .setQualification(new QualificationBuilder().setId(1).build())
                .setProject(new ProjectBuilder().setId(1).build())
                .build());
        
        assertEquals("test delete ", true, result);
        connection.rollback();
    }

    @Test
    public void testFindAll() throws SQLException {
        Set<ProjectQualification> instance = dao.findAll(connection);
        assertEquals("Find all test", false, instance.isEmpty());
        
    }

    @Test
    public void testUpdate() throws SQLException {
        boolean result = dao.update(connection, 
                new ProjectQualificationBuilder()
                .setQualification(new QualificationBuilder().setId(1).build())
                .setProject(new ProjectBuilder().setId(2).build())
                .setCount(100)
                .build());
        
        assertEquals("test update ", true, result);
        connection.rollback();
    }   
}
