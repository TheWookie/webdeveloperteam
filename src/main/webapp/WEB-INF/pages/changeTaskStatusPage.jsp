<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="bundles.bundle" />

<html>
<head>
    <title>Welcome</title>
    <style type="text/css">
        <%@include file="/resources/css/bootstrap.css"%>
    </style>
</head>
<body style="background-color: whitesmoke">
    <div style="border-style:solid; border-color:#428bca; background-color:#428bca; margin: auto;" id="header">
            <div>
                <h2 style="text-align: center; font-size: 3.8vw;">
                    <fmt:message key="head.dev.page"/>
                </h2>
            </div>
        </div>
        
        <div id="menu-block" class="well" style="width: 20%; float: left;">
            <div id="menu">
                <!--<button onclick="history.go(-1);return true;" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="button">
                    <p style="font-size: 2vw;">
                    	<fmt:message key="button.back"/>
                    </p>
                </button>-->
				<form style="display: inline" action="./Controller" method="POST">
                <button name="command" value="BACK_TO_MAIN" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="submit">
                    <p style="font-size: 2vw;">    
                    	<fmt:message key="button.back"/>
                    </p>
                </button>
                </form>
            </div>
        </div>
        
        <div id="main" class="container" style="width: 60%; float: left">
            <form action="./Controller" method="POST" class="form-signin">
            <input type="hidden" name="taskId" value="${taskId}"/>
            <input type="hidden" name="command" value="CHANGE_TASK_STATUS"/>
                
            <select style="margin-top: 3%;" name="selectedStatus" class="form-control" > <!--onchange="this.form.submit()"-->
                            <c:forEach var="status" items="${statusList}">
                                <option value="${status.id}">
                                    ${status.name}
                                </option>
                            </c:forEach>    
            </select>
            <button style="margin-top: 3%; width: 25%;" class="btn btn-lg btn-primary btn-block" type="submit">
                <fmt:message key="form.submit"/>
            </button>
            </form>
            
        </div>         
</body>
</html>