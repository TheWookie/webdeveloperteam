    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="bundles.bundle" />

<html>
<head>
    <title>Welcome</title>
    <style type="text/css">
        <%@include file="/resources/css/bootstrap.css"%>
    </style>
</head>
<body style="background-color: whitesmoke">
    <div style="border-style:solid; border-color:#428bca; background-color:#428bca; margin: auto;" id="header">
            <div>
                <h2 style="text-align: center; font-size: 3.8vw;">
                    <fmt:message key="head.manager.page"/>
                </h2>
            </div>
        </div>
        
        <div id="menu-block" class="well" style="width: 20%; float: left;">
            <div id="menu">
                <form style="display: inline" action="./Controller" method="POST">
                <button name="command" value="SHOW_PROFILE" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="submit">
                    <p style="font-size: 2vw;">    
                    	<fmt:message key="button.profile"/>
                    </p>
                </button>
                <button name="command" value="SHOW_PROJECTS_IN_PROGRESS" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="submit">
                    <p style="font-size: 2vw;">    
                    	<fmt:message key="button.show.projects"/>
                    </p>
                </button>
                <button name="command" value="LOGOUT" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="submit">
                    <p style="font-size: 2vw;">    
                    	<fmt:message key="button.logout"/>
                    </p>	
                </button>
                <!--<h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <a href="./Controller?command=SHOW_PROFILE"><fmt:message key="button.profile"/></a>
                </h3>
                <h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <a href="./Controller?command=SHOW_PROJECTS_IN_PROGRESS"><fmt:message key="button.show.projects"/></a>
                </h3>
                <h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <a href="./Controller?command=LOGOUT"><fmt:message key="button.logout"/></a>
                </h3>-->
                </form>
            </div>
        </div>
        
        <div id="main" class="container" style="width: 60%; float: left">
            <h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <fmt:message key="label.projects.new"/>
            </h3>

            <table class="table">
                <tr> 
                    <th><fmt:message key="label.name"/></th>
                    <th><fmt:message key="label.deadline"/></th>
                    <th><fmt:message key="label.details"/></th>
                    
                </tr>
                <c:forEach var="project" items="${projectList}">
                <tr>
                    <td>${project.name}</td>
                    <td>${project.deadline}</td>
                    <td>
                    	<form action="./Controller" method="POST" class="form-signin">
                    	<input type="hidden" name="projectId" value="${project.id}"/>
                    	<button name="command" value="PROCESS_PROJECT" style="margin-top: 3%;" 
                                class="btn btn-default btn-xs" type="submit">
                            <fmt:message key="label.process"/>
                        </button>
                        </form> 
                    <!--<td><a href="./Controller?command=PROCESS_PROJECT&projectId=${project.id}">
                            <fmt:message key="label.process"/>
                        </a>-->
                    </td>
                    
                </tr>
                </c:forEach>
            </table>
        </div>         
</body>
</html>