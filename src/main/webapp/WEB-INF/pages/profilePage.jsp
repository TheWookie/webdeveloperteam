<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="bundles.bundle" />
<!DOCTYPE html>

<html>
    <head>
        <style type="text/css">
            <%@include file="/resources/css/bootstrap.css"%>
        </style>
        <title>Profile</title>
    </head>
    <body style="background-color: whitesmoke">
        <div style="border-style:solid; border-color:#428bca; background-color:#428bca; margin: auto;" id="header">
            <div> 
                <h2 style="text-align: center; font-size: 3.8vw;"><fmt:message key="head.profile"/></h2>
            </div>
        </div>
        
        <div id="menu-block" class="well" style="width: 20%; float: left;">
            <div id="menu">
                <button onclick="history.go(-1);return true;" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="button">
                    <p style="font-size: 2vw;">    
                    	<fmt:message key="button.back"/>
                    </p>
                </button>
                <!--<h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <a onclick="history.go(-1);return true;"><fmt:message key="button.back"/></a>
                </h3>-->
            </div>
        </div>
        
        <div id="main" class="container" style="width: 60%; float: left">
            <h3 style="font-size: 2.8vw;"><fmt:message key="label.info"/></h3>
            <h5 style="font-size: 1.5vw;"><fmt:message key="label.name"/>: ${user.name}</h5>
            <h5 style="font-size: 1.5vw;"><fmt:message key="label.surname"/>: ${user.surname}</h5>
            <h5 style="font-size: 1.5vw;"><fmt:message key="label.email"/>: ${user.email}</h5>
            <hr color="black" style="float: none; width: 100%"/>
            <h5 style="font-size: 1.5vw;"><fmt:message key="label.login"/>: ${user.login}</h5>
            <h5 style="font-size: 1.5vw;"><fmt:message key="label.role"/>: ${user.role.name}</h5>
        </div>
    </body>
</html>
