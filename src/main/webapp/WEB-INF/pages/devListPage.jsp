<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="bundles.bundle" />

<html>
<head>
    <title>Welcome</title>
    <style type="text/css">
        <%@include file="/resources/css/bootstrap.css"%>
    </style>
</head>
<body style="background-color: whitesmoke">
    <div style="border-style:solid; border-color:#428bca; background-color:#428bca; margin: auto;" id="header">
            <div>
                <h2 style="text-align: center; font-size: 3.8vw;">
                    <fmt:message key="head.devList.page"/>
                </h2>
            </div>
        </div>
        
        <div id="menu-block" class="well" style="width: 20%; float: left;">
            <div id="menu">
                <button onclick="history.go(-1);return true;" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="button">
                    <p style="font-size: 2vw;">    
                    	<fmt:message key="button.back"/>
                    </p>
                </button>
                <!--<h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <a onclick="history.go(-1);return true;"><fmt:message key="button.back"/></a>
                </h3>-->
            </div>
        </div>
        
        <div id="main" class="container" style="width: 60%; float: left">
           
                <table class="table">
                <tr> 
                    <th><fmt:message key="label.name"/></th>
                    <th><fmt:message key="label.surname"/></th>
                    <th><fmt:message key="label.qualification"/></th>
                    <th><fmt:message key="label.age"/></th>
                    <th><fmt:message key="label.spendedTime"/></th>
                </tr>
                <c:forEach var="personProject" items="${personProjectList}">
                <tr>
                    <td>${personProject.key.person.name}</td>
                    <td>${personProject.key.person.surname}</td>
                    <td>${personProject.key.person.qualification.name}</td>  
                    <td>${personProject.value}</td>
                    <td>${personProject.key.time}</td> 
                </tr>
                </c:forEach>
                </table>
        </div>         
</body>
</html>