<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="bundles.bundle" />

<html>
<head>
    <title>Welcome</title>
    <style type="text/css">
        <%@include file="/resources/css/bootstrap.css"%>
    </style>
</head>
<body style="background-color: whitesmoke">
    <div style="border-style:solid; border-color:#428bca; background-color:#428bca; margin: auto;" id="header">
            <div>
                <h2 style="text-align: center; font-size: 3.8vw;">
                    <fmt:message key="head.project.settings"/>
                </h2>
            </div>
        </div>
        
        <div id="menu-block" class="well" style="width: 20%; float: left;">
            <div id="menu">
                <button onclick="history.go(-1);return true;" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="button">
                    <p style="font-size: 2vw;">    
                    	<fmt:message key="button.back"/>
                    </p>
                </button>
<!--                <h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <a onclick="history.go(-1);return true;"><fmt:message key="button.back"/></a>
                </h3>-->
            </div>
        </div>
        
        <div id="main" class="container" style="width: 60%; float: left">
            <form action="./Controller" method="POST" class="form-signin">
            <input type="hidden" name="projectId" value="${projectId}"/>

            <h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <fmt:message key="label.tasks"/>
            </h3>
            <table class="table">
                <tr> 
                    <th><fmt:message key="label.text"/></th>
                    <!--<th><fmt:message key="label.action"/></th>-->
                </tr>
                <c:forEach var="task" items="${taskList}">
                <tr>
                    <td>${task.text}</td>
                    <!--<td>
                        <form action="./Controller" method="POST" class="form-signin">
                            <input type="hidden" name="taskId" value="${task.id}">
                            <button name="command" value="CLIENT_DELETE_TASK" style="margin-top: 3%;" 
                                    class="btn btn-default btn-xs" type="button">
                                <fmt:message key="label.delete"/>
                            </button>
                        </form>
                    </td>-->
                </tr>
                </c:forEach>
            </table>
            <button style="margin-top: 3%; width: 25%;" class="btn btn-lg btn-primary btn-block" 
                    type="submit" name="command" value="ADD_TASK">
                <fmt:message key="button.add"/>
            </button>
                
            <h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                <fmt:message key="label.developers"/>
            </h3>
            <table class="table">
                <tr> 
                    <th><fmt:message key="label.qualification"/></th>
                    <th><fmt:message key="label.count"/></th>
                </tr>
                <c:forEach var="projectQualification" items="${projectQualificationList}">
                <tr>
                    <td>${projectQualification.qualification.name}</td>
                    <td>${projectQualification.count}</td>
                </tr>
                </c:forEach>
            </table>
            <button style="margin-top: 3%; width: 25%;" class="btn btn-lg btn-primary btn-block" 
                    type="submit" name="command" value="ADD_PROJECT_QUALIFICATION">
                <fmt:message key="button.add"/>
            </button>  
            
            <button style="margin-top: 3%;" class="btn btn-lg btn-primary btn-block" type="submit"
                    name="command" value="SUBMIT_FILLED_PROJECT">
                <fmt:message key="form.submit"/>
            </button>
            </form>
        </div>         
</body>
</html>