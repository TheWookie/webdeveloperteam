<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ex" uri="/WEB-INF/numberformatter.tld"%>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="bundles.bundle" />

<html>
<head>
    <title>Welcome</title>
    <style type="text/css">
        <%@include file="/resources/css/bootstrap.css"%>
    </style>
</head>
<body style="background-color: whitesmoke">
    <div style="border-style:solid; border-color:#428bca; background-color:#428bca; margin: auto;" id="header">
            <div>
                <h2 style="text-align: center; font-size: 3.8vw;">
                    <fmt:message key="head.manager.page"/>
                </h2>
            </div>
        </div>
        
        <div id="menu-block" class="well" style="width: 20%; float: left;">
            <div id="menu">
            	<form style="display: inline" action="./Controller" method="POST">
                <button name="command" value="BACK_TO_MAIN" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="submit">
                    <p style="font-size: 2vw;">    
                    	<fmt:message key="button.back"/>
                    </p>
                </button>
                </form>
<!--                <h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <a onclick="history.go(-1);return true;"><fmt:message key="button.back"/></a>
                </h3>-->
            </div>
        </div>
        
        <div id="main" class="container" style="width: 60%; float: left">
            <h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <fmt:message key="label.projects"/>
            </h3>
            
                <table class="table">
                <tr> 
                    <th><fmt:message key="label.name"/></th>
                    <th><fmt:message key="label.deadline"/></th>
                    <th><fmt:message key="label.status"/></th>
                    <th><fmt:message key="label.cost"/></th>
                    <th><fmt:message key="label.developers"/></th>
                    <th><fmt:message key="label.tasks"/></th>
                    <th><fmt:message key="label.action"/></th>
                </tr>
                <c:forEach var="project" items="${projectList}">
                <tr>
                    <td>${project.name}</td>
                    <td>${project.deadline}</td> 
                    <td>${project.status.name}</td>    
                    <td><ex:formatNumber>${project.cost}</ex:formatNumber></td>
                    <td>
                    	<form action="./Controller" method="POST" class="form-signin">
                    	<input type="hidden" name="projectId" value="${project.id}"/>
                    	<button name="command" value="SHOW_DEV_LIST" style="margin-top: 3%;" 
                                class="btn btn-default btn-xs" type="submit">
                            <fmt:message key="label.show"/>
                        </button>
                        </form> 
                    </td>
                    <td>
                    	<form action="./Controller" method="POST" class="form-signin">
                    	<input type="hidden" name="projectId" value="${project.id}"/>
                    	<button name="command" value="SHOW_TASK_LIST" style="margin-top: 3%;" 
                                class="btn btn-default btn-xs" type="submit">
                            <fmt:message key="label.show"/>
                        </button>
                        </form> 
                    </td>
                    <td>
                    	<form action="./Controller" method="POST" class="form-signin">
                    	<input type="hidden" name="projectId" value="${project.id}"/>
                    	<button name="command" value="SET_PROJECT_DONE" style="margin-top: 3%;" 
                                class="btn btn-default btn-xs" type="submit">
                            <fmt:message key="label.set.done"/>
                        </button>
                        </form>
                    </td>
                    <!--<td><a href="./Controller?command=SHOW_DEV_LIST&projectId=${project.id}">
                            <fmt:message key="label.show"/>
                        </a>
                    </td>
                    <td><a href="./Controller?command=SHOW_TASK_LIST&projectId=${project.id}">
                            <fmt:message key="label.show"/>
                        </a>
                    </td>
                    <td><a href="./Controller?command=SET_PROJECT_DONE&projectId=${project.id}">
                            <fmt:message key="label.set.done"/>
                        </a>
                    </td>-->
                </tr>
                </c:forEach>
                </table>
                
                <!-- PAGING -->
                <!--<hr align="left" color="black" style="float: none; width: 100%;"/>
                <div align="center" id="scroll">
                	<form action="./Controller" method="POST" class="form-signin">
                	<ul class="list-inline" style="list-style-type: none; padding: 0; margin-left: 0;">
                		<li>
                		<button name="command" value="" style="margin-top: 3%;" 
                                class="btn btn-default btn-xs" type="submit">
                            <-
                        </button>
                		</li>
                		<li> 1 </li>
                		<li>
                		<button name="command" value="" style="margin-top: 3%;" 
                                class="btn btn-default btn-xs" type="submit">
                            ->
                        </button>
                		</li>
                	</ul>
                	</form>-->
                </div>
                
        </div>         
</body>
</html>