<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ex" uri="/WEB-INF/numberformatter.tld"%>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="bundles.bundle" />

<html>
<head>
    <title>Welcome</title>
    <style type="text/css">
        <%@include file="/resources/css/bootstrap.css"%>
    </style>
</head>
<body style="background-color: whitesmoke">
    <div style="border-style:solid; border-color:#428bca; background-color:#428bca; margin: auto;" id="header">
            <div>
                <h2 style="text-align: center; font-size: 3.8vw;">
                    <fmt:message key="head.consumer.page"/>
                </h2>
            </div>
        </div>
        
        <div id="menu-block" class="well" style="width: 20%; float: left;">
            <div id="menu">
                <form style="display: inline" action="./Controller" method="POST">
                <button name="command" value="SHOW_PROFILE" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="submit">
                    <!--<h2 style="text-align: center; font-size: 1.8vw;">-->   
                    <p style="font-size: 2vw;">
                    	<fmt:message key="button.profile"/>
                    </p>
                    <!--</h2>-->
                </button>
                <button name="command" value="CREATE_PROJECT" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="submit">
                    <!--<h5 style="text-align: center;">-->   
                    <p style="font-size: 2vw;"> 
                    	<fmt:message key="button.create.project"/>
                    </p>		
                    <!--</h5>-->
                </button>
                <button name="command" value="LOGOUT" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="submit">
                    <!--<h2 style="text-align: center; font-size: 1.8vw;">-->
                    <p style="font-size: 2vw;">
                    	<fmt:message key="button.logout"/>
                    </p>
                    <!--</h2>-->
                </button>
                <!--<h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <a href="./Controller?command=SHOW_PROFILE"><fmt:message key="button.profile"/></a>
                </h3>
                <h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <a href="./Controller?command=CREATE_PROJECT"><fmt:message key="button.create.project"/></a>
                </h3>
                <h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <a href="./Controller?command=LOGOUT"><fmt:message key="button.logout"/></a>
                </h3>-->
                </form>
            </div>
        </div>
        
        <div id="main" class="container" style="width: 60%; float: left">
            <h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <fmt:message key="label.projects"/>
            </h3>
            
            	<div style="height: 400px; overflow: auto;">
                <table class="table table-striped">
                <tr> 
                    <th><fmt:message key="label.name"/></th>
                    <th><fmt:message key="label.deadline"/></th>
                    <th><fmt:message key="label.status"/></th>
                    <th><fmt:message key="label.cost"/></th>
                    <th><fmt:message key="label.developers"/></th>
                    <th><fmt:message key="label.tasks"/></th>
                    <th><fmt:message key="label.action"/></th>
                </tr>
                <c:forEach var="project" items="${projectList}">
                <tr>
                    <td>${project.name}</td>
                    <td>${project.deadline}</td> 
                    <td>${project.status.name}</td>    
                    <td><ex:formatNumber>${project.cost}</ex:formatNumber></td>
                    <td>
                    	<form action="./Controller" method="POST" class="form-signin">
                    	<input type="hidden" name="projectId" value="${project.id}"/>
                    	<button name="command" value="SHOW_DEV_LIST" style="margin-top: 3%;" 
                                class="btn btn-default btn-xs" type="submit">
                            <fmt:message key="label.show"/>
                        </button>
                    	<!--<a href="./Controller?command=SHOW_DEV_LIST&projectId=${project.id}">
           
                            <fmt:message key="label.show"/>
                        </a>-->
                        </form>
                    </td>
                    <td>
                    	<form action="./Controller" method="POST" class="form-signin">
                    	<input type="hidden" name="projectId" value="${project.id}"/>
                    	<button name="command" value="SHOW_TASK_LIST" style="margin-top: 3%;" 
                                class="btn btn-default btn-xs" type="submit">
                            <fmt:message key="label.show"/>
                        </button>
                    <!--<a href="./Controller?command=SHOW_TASK_LIST&projectId=${project.id}">
                            <fmt:message key="label.show"/>
                        </a>-->
                        </form>
                    </td>
                    <td>
                        <form action="./Controller" method="POST" class="form-signin">
                        <input type="hidden" name="projectId" value="${project.id}"/>
                        <!--<a href="./Controller?command=FILL_PROJECT">
                            <fmt:message key="label.update"/>
                        </a>-->
                            <button name="command" value="CLIENT_UPDATE_PROJECT" style="margin-top: 3%;" 
                                    class="btn btn-default btn-xs" type="submit">
                                <fmt:message key="label.update"/>
                            </button>
                        </form>
                    </td>
                </tr>
                </c:forEach>
                </table>
                </div>
        </div>         
</body>
</html>