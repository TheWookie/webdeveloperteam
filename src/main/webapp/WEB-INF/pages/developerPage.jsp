<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="bundles.bundle" />

<html>
<head>
    <title>Welcome</title>
    <style type="text/css">
        <%@include file="/resources/css/bootstrap.css"%>
    </style>
</head>
<body style="background-color: whitesmoke">
    <div style="border-style:solid; border-color:#428bca; background-color:#428bca; margin: auto;" id="header">
            <div>
                <h2 style="text-align: center; font-size: 3.8vw;">
                    <fmt:message key="head.dev.page"/>
                </h2>
            </div>
        </div>
        
        <div id="menu-block" class="well" style="width: 20%; float: left;">
            <div id="menu">
                <form style="display: inline" action="./Controller" method="POST">
                <button name="command" value="SHOW_PROFILE" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="submit">
                    <p style="font-size: 2vw;">    
                    	<fmt:message key="button.profile"/>
                    </p>
                </button>
                <button name="command" value="LOGOUT" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="submit">
                    <p style="font-size: 2vw;">
                    	<fmt:message key="button.logout"/>
                    </p>
                </button>
                </form>
            </div>
        </div>
        
        <c:if test = "${not empty taskList}">
        <div id="main" class="container" style="width: 60%; float: left">
            <!--<form action="./Controller" method="POST" class="form-signin">
            <input type="hidden" name="projectId" value="${projectId}"/>
            <input type="hidden" name="command" value="CHANGE_TASK_STATUS"/>-->
                
           
            <table class="table">
                <tr> 
                    <th><fmt:message key="label.text"/></th>
                    <th><fmt:message key="label.status"/></th>
                    <!--<th><fmt:message key="label.action"/></th>-->
                    <th><fmt:message key="label.action"/></th>
                </tr>
                <c:forEach var="task" items="${taskList}">
                <tr>
                    <td>${task.text}</td>
                    <td>${task.status.name}</td>
                    <td>
                    	<form action="./Controller" method="POST" class="form-signin">
                    	<input type="hidden" name="taskId" value="${task.id}"/>
                    	<button name="command" value="SHOW_TASK_STATUS" style="margin-top: 3%;" 
                                class="btn btn-default btn-xs" type="submit">
                            <fmt:message key="label.change"/>
                        </button>
                        </form> 
                    </td>
                    <!--<td>
                        <select name="select${task.id}" class="form-control" onchange="this.form.submit()">
                            <c:forEach var="status" items="${statusList}">
                                <c:if test = "${task.status.id == status.id}">
                                    <option value="${status.id}" selected>
                                        ${status.name}
                                    </option>
                                </c:if>
                                <c:if test = "${task.status.id != status.id}">
                                <option value="${status.id}">
                                    ${status.name}
                                </option>
                                </c:if>
                            </c:forEach>    
                        </select>
                    </td>-->
                    <!--<td>
                        <a href="./Controller?command=SHOW_TASK_STATUS&taskId=${task.id}">
                            <fmt:message key="label.change"/>
                        </a>
                    </td>-->
                </tr>
                </c:forEach>
            </table>
            <!--</form>-->
                
            <hr color="black" style="float: none; width: 100%"/>
            
            <form action="./Controller" method="POST" class="form-signin">
                <input type="hidden" name="projectId" value="${projectId}"/>
                
                 <div class="col-lg-6">
                    <div class="input-group">
                        <input type="text" name="time" class="form-control" placeholder="Time:">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary"
                                        type="submit" name="command" value="CHECK_IN">
                                <fmt:message key="button.checkIn"/>
                            </button>
                         </span>
                    </div>
                </div>
                
                <div>
                	<p style="font-size: 1.8vw;">
                		<fmt:message key="label.total.time"/>: ${time}
                	</p>
                </div>
                <!--<input type="text" name="time"/>
                <button style="margin-top: 3%; width: 25%;" class="btn btn-lg btn-primary btn-block" 
                        type="submit" name="command" value="CHECK_IN">
                    <fmt:message key="button.checkIn"/>
                </button>-->
            </form>
        </div>  
        </c:if>
        
        <c:if test = "${empty taskList}">
            <div>
                <h3 style="width: 100%; font-size: 2.8vw;">
                    <fmt:message key="label.developer.noProject"/>
                </h3>
            </div>
        </c:if>        
</body>
</html>