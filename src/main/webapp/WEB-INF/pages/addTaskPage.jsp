<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="bundles.bundle" />

<html>
<head>
    <title>Welcome</title>
    <style type="text/css">
        <%@include file="/resources/css/bootstrap.css"%>
    </style>
</head>
<body style="background-color: whitesmoke">
    <div style="border-style:solid; border-color:#428bca; background-color:#428bca; margin: auto;" id="header">
            <div>
                <h2 style="text-align: center; font-size: 3.8vw;">
                    <fmt:message key="head.task.settings"/>
                </h2>
            </div>
        </div>
        
        <div id="menu-block" class="well" style="width: 20%; float: left;">
            <div id="menu">
                <button onclick="history.go(-1);return true;" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="button">
                    <p style="font-size: 2vw;">
                    	<fmt:message key="button.back"/>
                   </p>
                </button>
                <!--<h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <a onclick="history.go(-1);return true;"><fmt:message key="button.back"/></a>
                    <!--<FORM><INPUT Type="button" VALUE="Back" onClick="history.go(-1);return true;"></FORM>-->
                </h3>
            </div>
        </div>
        
        <div id="main" class="container" style="width: 60%; float: left">
            <form action="./Controller" method="POST">
                <input type="hidden" name="command" value="SUBMIT_ADD_TASK"/>
                <input type="hidden" name="projectId" value="${projectId}"/>
                
                <label for="comment"><fmt:message key="label.text"/></label>
                <textarea name="taskText" class="form-control" rows="5" id="comment"></textarea>
                <button style="margin-top: 3%; width: 25%;" class="btn btn-lg btn-primary btn-block" 
                        type="submit"><fmt:message key="form.submit"/></button>
            </form>
        </div>         
</body>
</html>