<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="bundles.bundle" />

<html lang="${language}" >
    <c:if test="${language==null}">
        <fmt:setBundle basename="bundles.bundle" var="language" scope="session"/>
    </c:if>
    <head>
    <title>Welcome</title>
    <style type="text/css">
        <%@include file="/resources/css/bootstrap.css"%>
    </style>
    </head>
    <body style="background-color: whitesmoke">
        <div style="border-style:solid; border-color:#428bca; background-color:#428bca; margin: auto;" id="header">
            <div>
                <h2 style="text-align: center; font-size: 3.8vw;">
                    <fmt:message key="label.autentication"/>
                </h2>
            </div>
        </div>
        
        <div id="menu-block" class="well" style="width: 20%; float: left;">
            <div id="menu">
                <p style="font-size: 2vw;">
                	<fmt:message key="label.choose.language"/>:
                </p>
                <form action="/DevTeam/">
                    <select class="form-control" id="language" name="language" onchange="submit()">
                        <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
                        <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>                        
                    </select>
                </form>
            </div>   
        </div>
    
        <div class="container">
            <form action="./Controller" method="POST" class="form-signin">
                <input type="hidden" name="command" value="USER_LOGIN"/>
                <div style="width: 50%; margin-left: 25%;">
                    <h2 class="form-signin-heading"><fmt:message key="label.signIn"/></h2>
                    <div>
                        <label for="inputEmail" class="sr-only"><fmt:message key="label.login"/></label>
                        <input type="login" name="login" id="inputEmail" class="form-control" placeholder=<fmt:message key="label.login"/> required="" autofocus="">
                    </div>
                    <div style="margin-top: 1%;">
                        <label for="inputPassword" class="sr-only"><fmt:message key="label.password"/></label>
                        <input type="password" name="password" id="inputPassword" class="form-control" placeholder=<fmt:message key="label.password"/> required="">
                    </div>
                    
                <button style="margin-top: 3%;" class="btn btn-lg btn-primary btn-block" type="submit"><fmt:message key="form.submit"/></button>
                <a href="/DevTeam/registration.jsp"><fmt:message key="label.registration"/></a>
                <div>
            </form>
        </div> 
</body>
</html>
