<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="bundles.bundle" />

<html>
<head>
    <title>Welcome</title>
    <style type="text/css">
        <%@include file="/resources/css/bootstrap.css"%>
    </style>
</head>
<body style="background-color: whitesmoke">
    <div style="border-style:solid; border-color:#428bca; background-color:#428bca; margin: auto;" id="header">
            <div>
                <h2 style="text-align: center; font-size: 3.8vw;">
                    <fmt:message key="head.registration"/>
                </h2>
            </div>
        </div>
        
        <div id="menu-block" class="well" style="width: 20%; float: left;">
            <div id="menu">
                <button onclick="history.go(-1);return true;" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="button">
                    <fmt:message key="button.back"/>
                </button>
                <!--<h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <a onclick="history.go(-1);return true;"><fmt:message key="button.back"/></a>
                    <!--<FORM><INPUT Type="button" VALUE="Back" onClick="history.go(-1);return true;"></FORM>-->
                </h3>
            </div>
        </div>
        
        <div id="main" class="container" style="width: 60%; float: left">
            <form action="./Controller" method="POST">
                <input type="hidden" name="command" value="SUBMIT_REGISTRATION"/>
                <div style="width: 50%; margin-left: 25%;">
                    <h2 class="form-signin-heading"><fmt:message key="label.registration.title"/>:</h2>
                
                	<div class="form-group" style="margin-top: 1%;">
  						<label style="width: 50%;">
  							<p style="font-size: 1.2vw;"><fmt:message key="label.name"/></p>
  						</label>
  						<input name="userName" id="userName" class="form-control" required="" autofocus="">
					</div>
					<div class="form-group" style="margin-top: 1%;">
  						<label style="width: 50%;">
  							<p style="font-size: 1.2vw;"><fmt:message key="label.surname" /></p>
  						</label>
  						<input name="userSurname" id="userSurname" class="form-control" required="" autofocus="">
					</div>
					<div class="form-group" style="margin-top: 1%;">
  						<label style="width: 50%;">
  							<p style="font-size: 1.2vw;"><fmt:message key="label.login"/></p>
  						</label>
  						<input name="login" id="login" class="form-control" required="" autofocus="">
					</div>
					<div class="form-group" style="margin-top: 1%;">
  						<label style="width: 50%;">
  							<p style="font-size: 1.2vw;"><fmt:message key="label.email"/></p>
  						</label>
  						<input name="userEmail" id="userEmail" class="form-control" required="" autofocus="">
					</div>
					<div class="form-group" style="margin-top: 1%;">
  						<label style="width: 50%;">
  							<p style="font-size: 1.2vw;"><fmt:message key="label.phone"/></p>
  						</label>
  						<input name="userPhone" id="userPhone" class="form-control" required="" autofocus="">
					</div>
					<div class="form-group" style="margin-top: 1%;">
  						<label style="width: 50%;">
  							<p style="font-size: 1.2vw;"><fmt:message key="label.birthday"/></p>
  						</label>
  						<input type="date" name="userBirthday" id="userBirthday" class="form-control" required="" autofocus=""/>
					</div>
					<div class="form-group" style="margin-top: 1%;">
  						<label style="width: 50%;">
  							<p style="font-size: 1.2vw;"><fmt:message key="label.password"/></p>
  						</label>
  						<input name="password" id="password" class="form-control" required="" autofocus="">
					</div>
                
                	<!-- <input style="margin-top: 3%;" name="userName" id="userName" class="form-control" placeholder=<fmt:message key="label.name"/> required="" autofocus="">
    				<input style="margin-top: 3%;" name="userSurname" id="userSurname" class="form-control" placeholder=<fmt:message key="label.surname"/> required="" autofocus="">
    				<input style="margin-top: 3%;" name="login" id="login" class="form-control" placeholder=<fmt:message key="label.login"/> required="" autofocus="">
    				<input style="margin-top: 3%;" name="userEmail" id="userEmail" class="form-control" placeholder=<fmt:message key="label.email"/> required="" autofocus="">
    				<input style="margin-top: 3%;" name="userPhone" id="userPhone" class="form-control" placeholder=<fmt:message key="label.phone"/> required="" autofocus="">
    				<input style="margin-top: 3%;" type="date" name="userBirthday" id="userBirthday" class="form-control" placeholder=<fmt:message key="label.birthday"/> required="" autofocus=""/>
    				<input style="margin-top: 3%;" name="password" id="password" class="form-control" placeholder=<fmt:message key="label.password"/> required="" autofocus="">
    				-->
    				<hr color="black" style="float: none; width: 100%"/>
    				<button style="margin-top: 5%;" class="btn btn-lg btn-primary btn-block" type="submit"><fmt:message key="form.submit"/></button>
            	</div>
            </form>
        </div>         
</body>
</html>