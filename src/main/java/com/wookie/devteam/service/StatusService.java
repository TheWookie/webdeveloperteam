/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wookie.devteam.service;

import com.wookie.devteam.dao.DaoFactory;
import com.wookie.devteam.entities.Status;
import com.wookie.devteam.service.transactions.TransactionManager;
import java.util.Set;

/**
 *
 * @author wookie
 */
public class StatusService {
    private DaoFactory factory;
    private TransactionManager transactionManager;

    public StatusService(DaoFactory factory, TransactionManager transactionManager) {
        this.factory = factory;
        this.transactionManager = transactionManager;
    }

    /**
     * Method finds all Status entries in database.
     * @return Collection of Status entities.
     */
    public Set<Status> getAll() {
        return transactionManager.performTransaction( 
            connection -> {
                return factory.createStatusDao().findAll(connection);
            }
        );
    }
}
