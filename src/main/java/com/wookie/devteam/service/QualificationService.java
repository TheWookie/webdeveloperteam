/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wookie.devteam.service;

import com.wookie.devteam.dao.DaoFactory;
import com.wookie.devteam.entities.Qualification;
import com.wookie.devteam.service.transactions.TransactionManager;
import java.util.Set;

/**
 *
 * @author wookie
 */
public class QualificationService {
    private DaoFactory factory;
    private TransactionManager transactionManager;
    
    public QualificationService(DaoFactory factory, TransactionManager transactionManager) {
        this.factory = factory;
        this.transactionManager = transactionManager;
    }

    
    /**
     * Method returns all qualifications from database;
     * @return Collection of Qualification entities.
     */
    public Set<Qualification> getAll() {
        return transactionManager.performTransaction( 
            connection -> {
                return factory.createQualificationDao().findAll(connection);
            }
        );
    }
}
