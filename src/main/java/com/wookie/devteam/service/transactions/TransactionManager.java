/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wookie.devteam.service.transactions;

/**
 * Interface for performing transactions on service level. 
 * @author wookie
 */
public interface TransactionManager {
    
    /**
     * The method should call action method with prepared data.
     * @param <T>
     * @param action Operator instance.
     * @return type T.
     */
    <T> T performTransaction(Operation<T> action);
}
