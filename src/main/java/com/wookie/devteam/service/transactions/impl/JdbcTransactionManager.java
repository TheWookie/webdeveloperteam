/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wookie.devteam.service.transactions.impl;

import com.wookie.devteam.dao.jdbc.JdbcDaoFactory;
import com.wookie.devteam.service.transactions.Operation;
import com.wookie.devteam.service.transactions.TransactionManager;
import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Implementation of TransactionManager.
 * @author wookie
 */
public class JdbcTransactionManager implements TransactionManager {
    private static final Logger logger = LogManager.getLogger(JdbcTransactionManager.class);
    private DataSource ds;
    
    public JdbcTransactionManager(DataSource ds) {
        this.ds = ds;
    }
    
    @Override
    public <T> T performTransaction(Operation<T> action) {
        Connection connection = null;
        T result = null;
        
        try {
            connection = ds.getConnection();
            connection.setAutoCommit(false);
            
            logger.info("Start transaction");
            result = action.execute(connection);
            
            connection.commit();
            logger.info("Transaction isolation level: " + connection.getTransactionIsolation());
        } catch (Exception ex) {
            logger.error("Exception during transaction " + ex);
            if (connection != null) {
                try {
                    logger.info("Rollback");
                    connection.rollback();
                } catch (SQLException ex1) {
                    throw new RuntimeException();
                }
            }
            
            throw new RuntimeException(ex.getMessage());
        } finally {
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    throw new RuntimeException();
                }
            }
        }
        
    return result;
    }
}
