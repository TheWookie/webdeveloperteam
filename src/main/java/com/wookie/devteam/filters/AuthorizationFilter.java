/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wookie.devteam.filters;

import com.wookie.devteam.constants.Attributes;
import com.wookie.devteam.constants.Commands;
import com.wookie.devteam.constants.ErrorMessages;
import com.wookie.devteam.constants.Roles;
import com.wookie.devteam.dao.jdbc.JdbcQualificationDao;
import com.wookie.devteam.entities.Person;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Servlet Filter implementation class.
 * Class checks if current user has needed rights.
 */
@WebFilter("/*") 
public class AuthorizationFilter implements Filter {
   private static final Logger logger = LogManager.getLogger(JdbcQualificationDao.class);
   
   private Set<String> managerRole = new HashSet<>();
   {
       managerRole.add(Roles.MANAGER_ROLE);
   }
   
   private Set<String> consumerRole = new HashSet<>();
   {
       consumerRole.add(Roles.CONSUMER_ROLE);
   }
   
   private Set<String> devRole = new HashSet<>();
   {
       devRole.add(Roles.DEV_ROLE);
   }
   
   private Set<String> anyRole = new HashSet<>();
   {
       anyRole.add(Roles.MANAGER_ROLE);
       anyRole.add(Roles.CONSUMER_ROLE);
       anyRole.add(Roles.DEV_ROLE);
   }
   
   private Map<String, Set<String>> commandRights = new HashMap<>();
   {
        commandRights.put(Commands.PROCESS_PROJECT_COMMAND, managerRole);
        commandRights.put(Commands.CREATE_PROJECT_COMMAND, consumerRole);
        commandRights.put(Commands.FILL_PROJECT_COMMAND, consumerRole);
        commandRights.put(Commands.ADD_TASK_COMMAND, consumerRole);
        commandRights.put(Commands.ADD_PROJECT_QUALIFICATION_COMMAND, consumerRole);
        commandRights.put(Commands.SUBMIT_ADD_TASK_COMMAND, consumerRole);
        commandRights.put(Commands.SUBMIT_ADD_PROJECT_QUALIFICATION_COMMAND, consumerRole);
        commandRights.put(Commands.SUBMIT_FILLED_PROJECT_COMMAND, consumerRole);
        commandRights.put(Commands.CHECK_IN_COMMAND, devRole);
        commandRights.put(Commands.CHANGE_TASK_STATUS_COMMAND, devRole);
        commandRights.put(Commands.SUBMIT_PROJECT_SETTINGS_COMMAND, managerRole);
        commandRights.put(Commands.SHOW_PROJECTS_IN_PROGRESS_COMMAND, managerRole);
        commandRights.put(Commands.SET_PROJECT_DONE_COMMAND, managerRole);
        commandRights.put(Commands.SHOW_TASK_STATUS_COMMAND, devRole);
        commandRights.put(Commands.SHOW_DEV_LIST_COMMAND, anyRole);
        commandRights.put(Commands.SHOW_TASK_LIST_COMMAND, anyRole);
   }
   
    /**
     * Default constructor. 
     */
    public AuthorizationFilter() {
        
    }

    /**
     * Method check rights of user to access a web page.
     * @param request
     * @param response
     * @param chain
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
            throws IOException, ServletException, RuntimeException {
        logger.info("Authorization filter started");
        
        String personRole = (String) ((HttpServletRequest)request).getSession()
                .getAttribute(Attributes.USER_ROLE_SESSION);
        String currentCommand = request.getParameter(Attributes.COMMAND);
        
        if(currentCommand != null) {
            try {
                Set<String> neededRole = commandRights.get(currentCommand);
                if(neededRole != null)
                    if(!neededRole.contains(personRole)) {
                        logger.error("Authorization rights failed");
                        throw new RuntimeException(ErrorMessages.BROKEN_RIGHTS);
                    }
                 
            } catch (NullPointerException e) {
                logger.error("Authorization filter exception");
                throw new RuntimeException(ErrorMessages.BROKEN_RIGHTS);
            }
        }
        
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("Authorization filter init");
    }

    @Override
    public void destroy() {
        logger.info("Authorization filter destroy");
    }
}
