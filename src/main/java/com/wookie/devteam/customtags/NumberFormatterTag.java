/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wookie.devteam.customtags;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DecimalFormat;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.SkipPageException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author wookie
 */
public class NumberFormatterTag extends SimpleTagSupport {
	public static final String SIGN = " $";
    private String number;

    public NumberFormatterTag() {
    }

    public void setNumber(String number) {
    	this.number = number;
    }

    @Override
    public void doTag() throws JspException, IOException {
    	StringWriter sw = new StringWriter();
    	getJspBody().invoke(sw);
    	
    	if(!sw.toString().isEmpty())
    		getJspContext().getOut().write(sw.toString() + SIGN);

    }
}
