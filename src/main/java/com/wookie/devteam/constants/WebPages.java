/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wookie.devteam.constants;

/**
 *
 * @author wookie
 */
public interface WebPages {
    String LOGIN_PAGE = "index.jsp";
    String CONSUMER_PAGE = "WEB-INF/pages/consumerPage.jsp";
    String MANAGER_PAGE = "WEB-INF/pages/managerPage.jsp";
    String DEVELOPER_PAGE = "WEB-INF/pages/developerPage.jsp";
    String DEV_LIST_PAGE = "WEB-INF/pages/devListPage.jsp";
    String TASK_LIST_PAGE = "WEB-INF/pages/taskListPage.jsp";
    String PROJECT_PROCESS_PAGE = "WEB-INF/pages/projectProcessPage.jsp";
    String CREATE_PROJECT_PAGE = "WEB-INF/pages/createProjectPage.jsp";
    String FILL_PROJECT_PAGE = "WEB-INF/pages/fillProjectPage.jsp";
    String ADD_TASK_PAGE = "WEB-INF/pages/addTaskPage.jsp";
    String ADD_PROJECT_QUALIFICATION_PAGE = "WEB-INF/pages/addProjectQualificationPage.jsp";
    String PROFILE_PAGE = "WEB-INF/pages/profilePage.jsp";
    String MANAGER_PROJECTS_PAGE = "WEB-INF/pages/managerProjectsPage.jsp";
    String CHANGE_TASK_STATUS_PAGE = "WEB-INF/pages/changeTaskStatusPage.jsp";
}
