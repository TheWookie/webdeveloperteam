/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wookie.devteam.constants;

/**
 *
 * @author wookie
 */
public interface ErrorMessages {
    String WRONG_PASSWORD = "Wrong password.";
    String EXISTING_PROJECT = "Such project is already exists.";
    String EXISTING_PERSON = "Such user is already exists";
    String WRONG_INPUT_DATA = "Wrong input information.";
    String UNAVAILABLE_FOR_UPDATE = "This project is unavailable for update.";
    String BROKEN_RIGHTS = "You don't have permissions for this page";
    String WRONG_WEB_PAGE = "There are no such web-page";
    String WRONG_FUTURE_DATE = "The inputed date was invalid. The date you choose must be in future.";
    String WRONG_PAST_DATE = "The inputed date was invalid. The date you choose must be in past.";
    String DEV_HAS_NO_PROJECT = "You have no avialable projects.";
    String WRONG_PHONE_NUMBER = "The inputed phone number is wrong.";
    String WRONG_EMAIL = "The emaill is wrong.";
    String WRONG_CREDENTIALS = "Wrong credentials";
}
