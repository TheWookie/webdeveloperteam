/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wookie.devteam.constants;

/**
 *
 * @author wookie
 */
public interface Attributes {
    String IN_PROGRESS_STATUS = "In progress";
    String FREE_PROJECT = "Processed";
    String DONE_PROJECT = "Done";
    String EMPTY_PROJECT = "Empty";
    String COMMAND = "command";
    String USER_SESSION = "currentUser";
    String USER_ROLE_SESSION = "currentUserRole";
    String USER_NAME = "userName";
    String USER_SURNAME = "userSurname";
    String USER_PHONE = "userPhone";
    String USER_EMAIL = "userEmail";
    String USER_BIRTHDAY = "userBirthday";
    String USER_LOGIN = "login";
    String USER_PASSWORD = "password";
    String USER_ID = "userId";
    String PROJECT_LIST = "projectList";
    String PERSON_PROJECT_LIST = "personProjectList";
    String PROJECT_QUALIFICATION_LIST = "projectQualificationList";
    String PROJECT_ID = "projectId";
    String PROJECT_NAME = "projectName";
    String PROJECT_DEADLINE = "projectDeadline";
    String PROJECT_COST = "projectCost";
    String DEV_LIST = "devList";
    String TASK_LIST = "taskList";
    String TEST_TEXT = "taskText";
    String QUALIFICATION_LIST = "qualificationList";
    String QUALIFICATION_ID = "qualificationId";
    String STATUS_LIST = "statusList";
    String COUNT = "count";
    String TIME = "time";
    String SELECT_DEVS = "selectDevs";
    String SELECT = "select";
    String SELECTED_STATUS = "selectedStatus";
    String USER = "user";
    String TASK_COST = "taskCost";
    String TASK_ID = "taskId";
}
