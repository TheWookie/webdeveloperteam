/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wookie.devteam.constants;

/**
 *
 * @author wookie
 */
public interface Roles {
    String DEV_ROLE = "Developer";
    String MANAGER_ROLE = "Manager";
    String CONSUMER_ROLE = "Client";
}
