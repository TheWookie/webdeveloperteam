/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wookie.devteam.context;

import com.wookie.devteam.constants.Services;
import com.wookie.devteam.dao.DaoFactory;
import com.wookie.devteam.service.PersonProjectService;
import com.wookie.devteam.service.PersonService;
import com.wookie.devteam.service.ProjectQualificationService;
import com.wookie.devteam.service.ProjectService;
import com.wookie.devteam.service.QualificationService;
import com.wookie.devteam.service.StatusService;
import com.wookie.devteam.service.TaskService;
import com.wookie.devteam.service.factory.ServiceFactory;
import com.wookie.devteam.service.transactions.impl.JdbcTransactionManager;
import com.wookie.devteam.service.transactions.TransactionManager;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author wookie
 */
@WebListener
public class ServletInitialContext implements ServletContextListener {
    private static final Logger logger = LogManager.getLogger(ServletInitialContext.class);
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.info("Context initialization started");
        
        DaoFactory factory = DaoFactory.getFactory();
        
        TransactionManager transactionManager = new JdbcTransactionManager(factory.getDataSource());
        
        ServiceFactory serviceFactory = new ServiceFactory();
        serviceFactory.setPersonProjectService(new PersonProjectService(factory, transactionManager));
        serviceFactory.setPersonService(new PersonService(factory, transactionManager));
        serviceFactory.setProjectQualificationService(new ProjectQualificationService(factory, transactionManager));
        serviceFactory.setProjectService(new ProjectService(factory, transactionManager));
        serviceFactory.setQualificationService(new QualificationService(factory, transactionManager));
        serviceFactory.setStatusService(new StatusService(factory, transactionManager));
        serviceFactory.setTaskService(new TaskService(factory, transactionManager));
        
        ServletContext servletContext = sce.getServletContext();
        
        servletContext.setAttribute(Services.FACTORY, serviceFactory);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        logger.info("Context Destroyed");
    }
    
}
