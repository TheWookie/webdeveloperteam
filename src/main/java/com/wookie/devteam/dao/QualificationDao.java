/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wookie.devteam.dao;

import com.wookie.devteam.entities.Qualification;

/**
 *
 * @author wookie
 */
public interface QualificationDao extends GenericDao<Qualification> {
    
}
