/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wookie.devteam.dao;

import com.wookie.devteam.entities.Role;

/**
 *
 * @author wookie
 */
public interface RoleDao extends GenericDao<Role> {
    
}
